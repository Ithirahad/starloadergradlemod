package me.jakev.examplemod;

import api.DebugFile;
import api.ModPlayground;
import api.listener.Listener;
import api.listener.events.weapon.PulseAddEvent;
import api.mod.StarLoader;
import api.mod.StarMod;
import api.utils.StarRunnable;

public class ExampleMod extends StarMod {
    public static void main(String[] args) { }
    @Override
    public void onGameStart() {
        setModName("ExampleMod");
        setModVersion("1.0");
        setModDescription("Example for modding starmade");
        setModAuthor("JakeV");
        setModSMVersion("0.202.101");
    }

    @Override
    public void onEnable() {
        DebugFile.log("My mod was enabled, cool!");
        new StarRunnable() {
            @Override
            public void run() {
                ModPlayground.broadcastMessage("Ran my star runnable");
            }
        }.runTimer(this,25*5);

        StarLoader.registerListener(PulseAddEvent.class, new Listener<PulseAddEvent>() {
            @Override
            public void onEvent(PulseAddEvent event) {
                ModPlayground.broadcastMessage(event.getPulse().getOwner().getName() + " Just shot a push pulse!");
            }
        }, this);
    }
}
